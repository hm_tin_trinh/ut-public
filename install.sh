#!/usr/bin/env bash

command_exists() {
	command -v "$@" >/dev/null 2>&1
}

error() {
	echo ${RED}"Error: $@"${RESET} >&2
}

setup_color() {
	# Only use colors if connected to a terminal
	if [ -t 1 ]; then
		RED=$(printf '\033[31m')
		GREEN=$(printf '\033[32m')
		YELLOW=$(printf '\033[33m')
		BLUE=$(printf '\033[34m')
		BOLD=$(printf '\033[1m')
		RESET=$(printf '\033[m')
	else
		RED=""
		GREEN=""
		YELLOW=""
		BLUE=""
		BOLD=""
		RESET=""
	fi
}

download_bin() {
  if [[ "$OSTYPE" =~ darwin ]]; then
    curl -L -o ut https://bitbucket.org/hm_tin_trinh/ut-public/downloads/darwin-ut
  fi

  if [[ "$OSTYPE" =~ linux ]]; then
    curl -L -o ut https://bitbucket.org/hm_tin_trinh/ut-public/downloads/linux-ut
  fi

  chmod +x ut
}

setup_path() {
  if [[ "$OSTYPE" =~ darwin ]]; then
    mv ut /usr/local/bin
  fi

  if [[ "$OSTYPE" =~ linux ]]; then
    mv ut $HOME/bin
  fi
  
}

main() {
  setup_color
  download_bin
  setup_path
}

main